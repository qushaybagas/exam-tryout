<?php
  session_start();
 if (!isset($_SESSION['user'])){
    header("Location:index.php");
  }
  require("conn.php");
  if (isset($_GET['mapel'])){
    $materi_mapel=$_GET['mapel'];
  }else{
    $materi_mapel="mtk";
  }

  if (isset($_GET['user_del'])){
    $user_del=$_GET['user_del'];
    $sql_user_delete="DELETE FROM t_user WHERE uid='".$user_del."'";
    mysql_query($sql_user_delete);
    header( 'Location: user.php') ;
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Admin Tryout</title>
    <meta name="description" content="">
    <meta name="author" content="">

    
    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="css/prettify.css" rel="stylesheet">
    <link href="css/bootstrap-fileupload.min.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="#">Admin Tryout</a>
      <div class="nav-collapse">
        <ul class="nav">
          <li class="dropdown"  class="active">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mata Pelajaran <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="mat.php">Matematika</a></li>
              <li><a href="ipa.php">IPA</a></li>
              <li><a href="bin.php">Bahasa Indonesia</a></li>
              <li><a href="big.php">Bahasa Inggris</a></li>
            </ul>
          </li>  
          <li><a href="materi.php">Materi</a></li> 
          <li><a href="tips.php">Tips</a></li>     
          <li class="active"><a href="user.php">Pengguna</a></li> 
        </ul>
      </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
  </div><!-- /.navbar-inner -->
</div><!-- /.navbar -->
</div>


<div class="container">

  <?php

  if(isset($_GET['id_materi'])){
    $sql_materi_edit="SELECT * FROM t_materi WHERE id_materi='$id_materi'";
    $hasil_materi_edit=mysql_query($sql_materi_edit);
    $data_materi_edit=mysql_fetch_array($hasil_materi_edit);
  }

?>

  <div class="modal-header">
    <h3>Tambah user</h3>
  </div>

  <form action="user.php" method="post">
  <div class="modal-body">
        <table>
          <tr>
            <td class="span7">

              <input class="span4" type="text" placeholder="Nama" name="nama_user" required>
            </td>
          </tr>
          <tr>
            <td>
              <input class="span4" type="email" placeholder="Email" name="email_user" required>
            </td>
          </tr>
          <tr>
            <td>
              <input class="span4" type="password" placeholder="Password" name="pass_user" required>
            </td>
          </tr>
        </table>
   
  </div>
  <div class="modal-footer">
    <button type="submit" name="save" value="save" class="btn btn-primary">Save</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
   </form>
</div>

<!-- Modal Soal Form -->
<div class="modal hide" id="ModaluserEdit" aria-hidden="true">
  <div class="modal-header">
    <h3>Edit user</h3>
  </div>

  <form action="user.php" method="post">
  <div class="modal-body">
        <table>
          <tr>
            <td >
              <input type="hidden" name="id_user" value=<?php echo $data_user_edit['id_user'];  ?>>
              <textarea class="span5" rows="1" name="judul_user"><?php echo $data_user_edit['bab']; ?>
              </textarea>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span5" rows="5" name="isi_user"><?php echo $data_user_edit['user']; ?>
              </textarea>
            </td>
          </tr>
        </table>
   
  </div>
  <div class="modal-footer">
    <button type="submit" name="edit" value="edit" class="btn btn-primary">Edit</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
   </form>
</div>

<script type="text/javascript" src="js/widgets.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-fileupload.min.js"></script>
<script src="js/application.js"></script>
<script src="js/holder.js"></script>
</body>
</html>