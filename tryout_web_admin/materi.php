<?php
 session_start();
 if (!isset($_SESSION['user'])){
    header("Location:index.php");
  }
  require("conn.php");
  if (isset($_GET['mapel'])){
    $materi_mapel=$_GET['mapel'];
  }else{
    $materi_mapel="mtk";
  }

  if (isset($_POST['judul_materi'])&&isset($_POST['input_materi_mapel'])){
    $judul_materi=$_POST['judul_materi'];
    $isi_materi=$_POST['isi_materi'];
    $file_x=$_FILES['input_materi_gbr'];
    $materi_mapel=$_POST['input_materi_mapel'];
    if ($_POST['input_materi_aksi']=="save"){
    	//mencari kode materi terakhir
	$sql_lihat_kode_materi="SELECT id_materi FROM t_materi WHERE id_mapel='".$materi_mapel."' ORDER BY id_materi DESC";
	$nomer_kode_materi_terakhir=mysql_fetch_array(mysql_query($sql_lihat_kode_materi));
    
	
	$allowedExts = array("jpg", "jpeg", "gif", "png","JPG","PNG");
	$file_x_nomer=$nomer_kode_materi_terakhir['id_materi']+1;
	$file_x_nama=$materi_mapel."_".$file_x_nomer;
    	$file_x_name="client/materi/".$file_x_nama.".png";
    	$file_x_exist=0;
	$ext_soal_gbr = end(explode(".", $file_x["name"]));
	if ((($file_x["type"] == "image/gif")
	|| ($file_x["type"] == "image/jpeg")
	|| ($file_x["type"] == "image/png")
	|| ($file_x["type"] == "image/pjpeg"))
	&& ($file_x["size"] < 1000000) //1MB
	&& in_array($ext_soal_gbr, $allowedExts)){
	  	if ($file_x["error"] > 0){
			header( 'Location: input_materi.php?mapel='.$materi_mapel ) ;
	  	}else{
	    		if (file_exists($file_x_name)){
	      		//do nothing
		    	}else{
		      move_uploaded_file($file_x["tmp_name"],$file_x_name);
		      $file_x_exist=1;
	  		}
	  	}
	}else{
		header( 'Location: input_materi.php?mapel='.$materi_mapel ) ;
	}
	if($isi_materi==null||$isi_materi==""){
      $sql_materi_input="INSERT INTO t_materi (id_mapel, bab, materi,gbr_materi)VALUES('$materi_mapel','$judul_materi',null,'$file_x_nama')";
      }else{
      $sql_materi_input="INSERT INTO t_materi (id_mapel, bab, materi,gbr_materi)VALUES('$materi_mapel','$judul_materi','$isi_materi',null)";
      }
      mysql_query($sql_materi_input);
    }elseif ($_POST['input_materi_aksi']=="edit"){
      $id_materi=$_POST['id_materi'];
      $sql_materi_edit="UPDATE t_materi SET bab='$judul_materi', materi='$isi_materi' WHERE id_materi='$id_materi'";
      mysql_query($sql_materi_edit);
    }elseif ($_POST['input_materi_aksi']=="delete"){
      $sql_materi_delete="DELETE FROM t_materi WHERE id_materi='4'";
      mysql_query($sql_materi_delete);
    }
  }


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Admin Tryout</title>
    <meta name="description" content="">
    <meta name="author" content="">

    
    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="css/prettify.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="#">Admin Tryout</a>
      <div class="nav-collapse">
        <ul class="nav">
          <li class="dropdown"  class="active">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mata Pelajaran <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="mat.php">Matematika</a></li>
              <li><a href="ipa.php">IPA</a></li>
              <li><a href="bin.php">Bahasa Indonesia</a></li>
              <li><a href="big.php">Bahasa Inggris</a></li>
            </ul>
          </li>  
          <li class="active"><a href="materi.php">Materi</a></li> 
          <li><a href="tips.php">Tips</a></li>     
          <li><a href="user.php">Pengguna</a></li> 
        </ul>
        
        <ul class="pull-right nav">
          <li><a href="logout.php"><i class="icon-off"></i> Logout</a></li> 
        </ul>
      </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
  </div><!-- /.navbar-inner -->
</div><!-- /.navbar -->
</div>


<div class="pagination pagination-centered pagination-large">
  <ul>
    <li><a href="materi.php?mapel=mtk">Matematika</a></li>
    <li><a href="materi.php?mapel=bin">B. Indonesia</a></li>
    <li><a href="materi.php?mapel=big">B. Inggris</a></li>
    <li><a href="materi.php?mapel=ipa">IPA</a></li>
  </ul>
</div>

<div class="container">

  <p style="margin-top:10px;">
    <a href="input_materi.php?mapel=<?php echo $materi_mapel ?>" class="btn btn-primary"><i class="icon-plus icon-white"></i> Materi</a>
  </p>
  <div class="accordion" id="accordion2">
    <?php
    $sql_materi="SELECT * FROM t_materi WHERE id_mapel='".$materi_mapel."'";
    $hasil_materi=mysql_query($sql_materi);
    $iMateri=1;
    while($data_materi=mysql_fetch_array($hasil_materi)){
    $id_materi=$data_materi['id_materi'];
    echo "<div class='accordion-group'>";
    echo "  <div class='accordion-heading'>";
    echo "    <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion2' href='#collapse".$iMateri."'>";
    echo          $data_materi['bab'];
    echo "    </a>";
    echo "    <div class='accordion-heading-menu'> 
                <a href='input_materi.php?materi_del=".$id_materi."&mapel=".$materi_mapel."'><i class='icon-trash'></i></a>
              </div>";
    echo "  </div>";
    echo "  <div id='collapse".$iMateri."' class='accordion-body collapse'>
              <div class='accordion-inner'>";
              if ($data_materi['materi']!=null||$data_materi['']!=""){
    echo          $data_materi['materi'];
    		}else{
    echo "<img src='client/materi/".$data_materi['gbr_materi'].".png'>";
    		}
    echo "    </div>
            </div>
          </div>";
    $iMateri++;
    }
    ?>
  </div>
</div>

<script type="text/javascript" src="js/widgets.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/application.js"></script>
<script src="js/holder.js"></script>
<script type="text/javascript">
</body>
</html>