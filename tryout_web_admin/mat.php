<?php
  session_start();

  if (!isset($_SESSION['user'])){
    header("Location:index.php");
  }else{
    require("conn.php");
    $mapel="MTK";
    if (isset($_GET['set'])){
      $kode_soal=$_GET['set'];
    }else{
      $kode_soal=1;
    }
  }



?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Admin Tryout</title>
    <meta name="description" content="">
    <meta name="author" content="">

    
    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="css/prettify.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="#">Admin Tryout</a>
      <div class="nav-collapse">
        <ul class="nav">
          <li class="dropdown"  class="active">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mata Pelajaran <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="mat.php">Matematika</a></li>
              <li><a href="ipa.php">IPA</a></li>
              <li><a href="bin.php">Bahasa Indonesia</a></li>
              <li><a href="big.php">Bahasa Inggris</a></li>
            </ul>
          </li>  
          <li><a href="materi.php">Materi</a></li> 
          <li><a href="tips.php">Tips</a></li>     
          <li><a href="user.php">Pengguna</a></li> 

        </ul>
        <ul class="pull-right nav">
          <li><a href="logout.php"><i class="icon-off"></i> Logout</a></li> 
        </ul>
      </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
  </div><!-- /.navbar-inner -->
</div><!-- /.navbar -->
</div>

<div class="pagination pagination-centered pagination-large">
  <ul>
    <li><a style="margin-top:5px;" href="admin.php?kurang=1&mapel=<?php echo $mapel; ?>" class="btn btn-primary"><i class="icon-minus icon-white"></i></a>
    </li>
  </ul>
  <ul>
    <?php 
      $sql_tampil_kode_soal="SELECT set_soal FROM t_kode_soal WHERE id_mapel='".$mapel."'";
      $query_tampil_kode_soal=mysql_query($sql_tampil_kode_soal);
      while ($data_tampil_kode_soal=mysql_fetch_array($query_tampil_kode_soal)){
        echo "<li><a href='mat.php?set=".$data_tampil_kode_soal['set_soal']."'>".$data_tampil_kode_soal['set_soal']."</a></li>";
      }
    ?>
  </ul>
  <ul>
    <li><a style="margin-top:5px;" href="admin.php?tambah=1&mapel=<?php echo $mapel; ?>" class="btn btn-primary"><i class="icon-plus icon-white"></i></a>
    </li>
  </ul>
</div>


<div class="container">
    <!-- nomer soal -->
    <div class="span1 bs-docs-sidebar">
      <a style="margin-top:5px;" href="input_soal.php?kode=<?php echo $kode_soal ?>&mapel=mtk" class="btn btn-primary"><i class="icon-plus icon-white"></i> Soal</a>
    
      <ul class="nav nav-list bs-docs-sidenav">
        <?php
          $sql="SELECT * FROM t_soal WHERE set_soal='".$kode_soal."' AND id_mapel='".$mapel."'";
          $jml_soal=mysql_num_rows(mysql_query($sql));
          for ($i=1;$i<=$jml_soal;$i++){
           echo "<li><a href='#".$i."'>".$i."</a></li>";
          }
        ?>
      </ul>

    </div>

    <div class="span9">
         
      <?php // inlude admin_isi
        include('admin_isi.php');
      ?>

      </div>
</div>


<script type="text/javascript" src="js/widgets.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/application.js"></script>
<script src="js/holder.js"></script>
</body>
</html>