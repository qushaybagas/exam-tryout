<?php
	session_start();
	if (!isset($_SESSION['user'])){
	   header("Location:index.php");
	}
	require("conn.php");
	$kode_soal=1;
	if (isset($_GET['tambah'])||isset($_GET['kurang'])){
		$kode_soal_tambah=$_GET['tambah'];
		$kode_soal_kurang=$_GET['kurang'];
		$mapel=$_GET['mapel'];

	    //mencari kode soal terakhir
	    $sql_lihat_kode_soal="SELECT set_soal FROM t_kode_soal WHERE id_mapel='".$mapel."' ORDER BY set_soal DESC";
	    $nomer_kode_soal_terakhir=mysql_fetch_array(mysql_query($sql_lihat_kode_soal));

		//tambah maupun kurangi kode soal
		if ($kode_soal_tambah>0){
			$nomer_kode_soal_terakhir_plus=$nomer_kode_soal_terakhir['set_soal']+1;
			$sql_kode_soal_tambah="INSERT INTO t_kode_soal(id_mapel,set_soal) VALUES ('$mapel','$nomer_kode_soal_terakhir_plus') ";
	 		mysql_query($sql_kode_soal_tambah);
	 		$kode_soal=$nomer_kode_soal_terakhir_plus;
		}elseif($kode_soal_kurang>0){
			$sql_kode_soal_kurang="DELETE FROM t_kode_soal WHERE id_mapel='".$mapel."' AND set_soal='".$nomer_kode_soal_terakhir['set_soal']."'";
	 		mysql_query($sql_kode_soal_kurang);
	 		$kode_soal=$nomer_kode_soal_terakhir['set_soal']-1;
		}

	}
	if (isset($_POST['input_soal_mapel'])&&isset($_POST['input_soal_soal'])){
		$mapel=$_POST['input_soal_mapel'];
	    $kode_soal=$_POST['input_soal_kode'];
	    $soal=$_POST['input_soal_soal'];
	    $pilgan_a=$_POST['input_soal_a'];
	    $pilgan_b=$_POST['input_soal_b'];
	    $pilgan_c=$_POST['input_soal_c'];
	    $pilgan_d=$_POST['input_soal_d'];
	    $option_pilgan=$_POST['option_pilgan'];
	    $solusi=$_POST['input_soal_solusi'];

	    //mencari nomer id_soal terakhir kemudian ditambah 1
	    $sql_lihat_nomer_soal="SELECT id_soal FROM t_soal ORDER BY id_soal DESC";
	    $nomer_soal_akhir=mysql_fetch_array(mysql_query($sql_lihat_nomer_soal));
	    $nomer_soal_akhir_plus=$nomer_soal_akhir['id_soal']+1;

	    //untuk upload gambar soal
	    $allowedExts = array("jpg", "jpeg", "gif", "png","JPG","PNG");
	    $file_soal=$_FILES["input_soal_soal_gbr"];
	    $file_pil1=$_FILES["input_soal_a_gbr"];
	    $file_pil2=$_FILES["input_soal_b_gbr"];
	    $file_pil3=$_FILES["input_soal_c_gbr"];
	    $file_pil4=$_FILES["input_soal_d_gbr"];
	    $file_solusi=$_FILES["input_soal_solusi_gbr"];

	    for ($i=0;$i<=5;$i++){
	    	if ($i==0){
	    		$file_x=$file_soal;
	    		if ($soal==null or $soal==""){
	    			$file_x_nama[$i]=$mapel."_".$kode_soal."_".$nomer_soal_akhir_plus;
	    			$file_x_name="client/soal/".$file_x_nama[$i].".png";
	    		}else{
	    			$file_x_nama[$i]=null;
	    		}
	    	}elseif($i==1){
	    		$file_x=$file_pil1;
	    		if ($pilgan_a==null or $pilgan_a==""){
	    		$file_x_nama[$i]=$mapel."_".$kode_soal."_".$nomer_soal_akhir_plus."_a";
	    		$file_x_name="client/jawaban/".$file_x_nama[$i].".png";
	    		}else{
	    			$file_x_nama[$i]=null;
	    		}
	    	}elseif($i==2){
	    		$file_x=$file_pil2;
	    		if ($pilgan_b==null or $pilgan_b==""){
	    		$file_x_nama[$i]=$mapel."_".$kode_soal."_".$nomer_soal_akhir_plus."_b";
	    		$file_x_name="client/jawaban/".$file_x_nama[$i].".png";
	    		}else{
	    			$file_x_nama[$i]=null;
	    		}
	    	}elseif($i==3){
	    		$file_x=$file_pil3;
	    		if ($pilgan_c==null or $pilgan_c==""){
	    		$file_x_nama[$i]=$mapel."_".$kode_soal."_".$nomer_soal_akhir_plus."_c";
	    		$file_x_name="client/jawaban/".$file_x_nama[$i].".png";
	    		}else{
	    			$file_x_nama[$i]=null;
	    		}
	    	}elseif($i==4){
	    		$file_x=$file_pil4;
	    		if ($pilgan_d==null or $pilgan_d==""){
	    		$file_x_nama[$i]=$mapel."_".$kode_soal."_".$nomer_soal_akhir_plus."_d";
	    		$file_x_name="client/jawaban/".$file_x_nama[$i].".png";
	    		}else{
	    			$file_x_nama[$i]=null;
	    		}
	    	}else{
	    		$file_x=$file_solusi;
	    	}
			$ext_soal_gbr = end(explode(".", $file_x["name"]));
			if ((($file_x["type"] == "image/gif")
			|| ($file_x["type"] == "image/jpeg")
			|| ($file_x["type"] == "image/png")
			|| ($file_x["type"] == "image/pjpeg"))
			&& ($file_x["size"] < 1000000) //1MB
			&& in_array($ext_soal_gbr, $allowedExts)){
			  	if ($file_x["error"] > 0){
	  				header( 'Location: input_soal.php?kode='.$kode_soal.'&mapel='.$mapel ) ;
			  	}else{
				    	if (file_exists($file_x_name)){
				      	//do nothing
				    	}else{
				      	move_uploaded_file($file_x["tmp_name"],$file_x_name);
			   		}
			  	}
			}else{
				header( 'Location: input_soal.php?kode='.$kode_soal.'&mapel='.$mapel ) ;
			}
		}

		//simpan soal ke DB
		if ($soal!=null or $soal!=""){
	    	$sql_save_soal="INSERT INTO t_soal(id_soal,id_mapel,set_soal,soal,gambar_soal,solusi,gambar_solusi) VALUES ('$nomer_soal_akhir_plus','$mapel','$kode_soal','$soal',null,null,null) ";
	    	}else{
	    	$sql_save_soal="INSERT INTO t_soal(id_soal,id_mapel,set_soal,soal,gambar_soal,solusi,gambar_solusi) VALUES ('$nomer_soal_akhir_plus','$mapel','$kode_soal',null,'$file_x_nama[0]',null,null) ";
	    	}
	 	mysql_query($sql_save_soal);

	 	//simpan pilgan ke DB
	 	for ($i=1;$i<=4;$i++){
	 		if($option_pilgan=="a"){
		 		if($i==1){
		 			$status_benar="b";
		 			$pilgan_x=$pilgan_a;
		 		}elseif($i==2){
		 			$status_benar="s";
		 			$pilgan_x=$pilgan_b;
		 		}elseif($i==3){
		 			$status_benar="s";
		 			$pilgan_x=$pilgan_c;
		 		}else{
		 			$status_benar="s";
		 			$pilgan_x=$pilgan_d;
		 		}
		 	}elseif ($option_pilgan=="b") {
		 		if($i==1){
		 			$status_benar="s";
		 			$pilgan_x=$pilgan_a;
		 		}elseif($i==2){
		 			$status_benar="b";
		 			$pilgan_x=$pilgan_b;
		 		}elseif($i==3){
		 			$status_benar="s";
		 			$pilgan_x=$pilgan_c;
		 		}else{
		 			$status_benar="s";
		 			$pilgan_x=$pilgan_d;
		 		}
		 	}elseif($option_pilgan=="c") {
		 		if($i==1){
		 			$status_benar="s";
		 			$pilgan_x=$pilgan_a;
		 		}elseif($i==2){
		 			$status_benar="s";
		 			$pilgan_x=$pilgan_b;
		 		}elseif($i==3){
		 			$status_benar="b";
		 			$pilgan_x=$pilgan_c;
		 		}else{
		 			$status_benar="s";
		 			$pilgan_x=$pilgan_d;
		 		}
		 	}elseif ($option_pilgan=="d") {
		 		if($i==1){
		 			$status_benar="s";
		 			$pilgan_x=$pilgan_a;
		 		}elseif($i==2){
		 			$status_benar="s";
		 			$pilgan_x=$pilgan_b;
		 		}elseif($i==3){
		 			$status_benar="s";
		 			$pilgan_x=$pilgan_c;
		 		}else{
		 			$status_benar="b";
		 			$pilgan_x=$pilgan_d;
		 		}
		 	}
		 	$file_pilgan_x=$file_x_nama[$i];
		 	if ($file_pilgan_x!=null){
	 		$sql_save_pilgan="INSERT INTO t_pilihan(id_soal,stat,pilihan,gambar_pilihan) VALUES 	('$nomer_soal_akhir_plus','$status_benar',null,'$file_pilgan_x')";
	 		}else{
	 		$sql_save_pilgan="INSERT INTO t_pilihan(id_soal,stat,pilihan,gambar_pilihan) VALUES ('$nomer_soal_akhir_plus','$status_benar','$pilgan_x',null)";
	 		}
	 		mysql_query($sql_save_pilgan);
	 	}
	  }

	  //delete soal dan pilgan dari DB
	  if (isset($_GET['mapel'])&&isset($_GET['del'])){
	  	$mapel=$_GET['mapel'];
	  	$soal_del=$_GET['del'];
	  	$kode_soal=$_GET['kode'];
	  	$sql_lihat_gambar_soal="SELECT gambar_soal FROM t_soal WHERE id_soal='$soal_del'";
	  	$query_gambar_soal=mysql_query($sql_lihat_gambar_soal);
	  	$gambar_soal_delete=mysql_fetch_array($query_gambar_soal);
	  	if (file_exists("client/soal/".$gambar_soal_delete['gambar_soal'].".png")){
	  		unlink("client/soal/".$gambar_soal_delete['gambar_soal'].".png");
	  	}
	  	
	  	$sql_lihat_gambar_pilihan="SELECT gambar_pilihan FROM t_pilihan WHERE id_soal='$soal_del'";
	  	$query_gambar_pilihan=mysql_query($sql_lihat_gambar_pilihan);
	  	while($gambar_pilihan_delete=mysql_fetch_array($query_gambar_pilihan)){
	  		if (file_exists("client/jawaban/".$gambar_pilihan_delete['gambar_pilihan'].".png")){
	  		unlink("client/jawaban/".$gambar_pilihan_delete['gambar_pilihan'].".png");
	  		}
	  	}

	  	$sql_soal_delete="DELETE FROM t_soal WHERE id_soal='$soal_del'";
      	mysql_query($sql_soal_delete);
      	$sql_pilgan_delete="DELETE FROM t_pilihan WHERE id_soal='$soal_del'";
      	mysql_query($sql_pilgan_delete);
	  }

	  //kembali ke halaman daftar soal
	  if ($mapel=="bin"||$mapel=="BIN"){
  		header( 'Location: bin.php?set='.$kode_soal ) ;
	  }elseif ($mapel=="big"||$mapel=="BIG"){
  		header( 'Location: big.php?set='.$kode_soal ) ;
	  }elseif ($mapel=="ipa"||$mapel=="IPA"){
  		header( 'Location: ipa.php?set='.$kode_soal ) ;
	  }else{
  		header( 'Location: mat.php?set='.$kode_soal ) ;
	  }
?>