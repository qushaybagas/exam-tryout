<?php
 session_start();
 if (!isset($_SESSION['user'])){
    header("Location:index.php");
  }
  require("conn.php");
  $mapel="MTK";
  $kode_soal=1;
  if (isset($_GET['kode'])&&isset($_GET['mapel'])){
    $kode_soal=$_GET['kode'];
    $mapel=$_GET['mapel'];
  }

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Admin Tryout</title>
    <meta name="description" content="">
    <meta name="author" content="">

    
    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/bootstrap-fileupload.min.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="css/prettify.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="#">Admin Tryout</a>
      <div class="nav-collapse">
        <ul class="nav">
          <li class="dropdown"  class="active">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mata Pelajaran <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="mat.php">Matematika</a></li>
              <li><a href="ipa.php">IPA</a></li>
              <li><a href="bin.php">Bahasa Indonesia</a></li>
              <li><a href="big.php">Bahasa Inggris</a></li>
            </ul>
          </li>  
          <li><a href="materi.php">Materi</a></li> 
          <li><a href="tips.php">Tips</a></li>     
          <li><a href="user.php">Pengguna</a></li> 
        </ul>
      </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
  </div><!-- /.navbar-inner -->
</div><!-- /.navbar -->
</div>

<div class="container">

<!-- Modal Soal Form -->
<div id="ModalSoal" >

  <form style="height: 380px;" method="post" action="admin.php" enctype="multipart/form-data">
  <div class="modal-header">
    <h3>Tambah Soal</h3>
  </div>
  <div class="modal-body">
        <table>
          <tr>
            <td class="span11" colspan="2">
              <textarea class="span9" name="input_soal_soal" rows="3" placeholder="Soal"></textarea>
            </td>
            <td class="span4">
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new"><i class="icon-folder-open"></i></span><span class="fileupload-exists"><i class="icon-folder-close"></i></span>
                <input type="file" name="input_soal_soal_gbr" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i></a>
              </div>
            </td>
          </tr>
          <tr>
            <td width="30px">
              <input type="radio" name="option_pilgan" id="optionPilganA" value="a" checked>
            </td>
            <td>
              <textarea class="span8"  name="input_soal_a" rows="2" placeholder="Pilihan Jawaban A"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new"><i class="icon-folder-open"></i></span><span class="fileupload-exists"><i class="icon-folder-close"></i></span>
                <input type="file" name="input_soal_a_gbr"/></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i></a>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="option_pilgan" id="optionPilganB" value="b" >
            </td>
            <td>
              <textarea class="span8" name="input_soal_b" rows="2" placeholder="Pilihan Jawaban B"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new"><i class="icon-folder-open"></i></span><span class="fileupload-exists"><i class="icon-folder-close"></i></span>
                <input type="file"  name="input_soal_b_gbr" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i></a>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="option_pilgan" id="optionPilganC" value="c">
            </td>
            <td>
              <textarea class="span8" name="input_soal_c" rows="2" placeholder="Pilihan Jawaban C"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new"><i class="icon-folder-open"></i></span><span class="fileupload-exists"><i class="icon-folder-close"></i></span>
                <input type="file"  name="input_soal_c_gbr"/></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i></a>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <input type="radio" name="option_pilgan" id="optionPilganD" value="d" >
            </td>
            <td>
              <textarea class="span8" name="input_soal_d" rows="2" placeholder="Pilihan Jawaban D"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new"><i class="icon-folder-open"></i></span><span class="fileupload-exists"><i class="icon-folder-close"></i></span>
                <input type="file"  name="input_soal_d_gbr"/></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i></a>
              </div>
            </td>
          </tr>
          
        </table>
  </div>
  <div class="modal-footer">
    <input name="input_soal_kode" type="hidden" value="<?php echo $kode_soal ?>"/>
    <input name="input_soal_mapel" type="hidden" value="<?php echo $mapel ?>"/>
    <button class="btn btn-primary">Save</button>
    <a href="admin.php" class="btn">Cancel</a>
  </div>
  </form>
</div>

<script type="text/javascript" src="js/widgets.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-fileupload.min.js"></script>
<script src="js/application.js"></script>
<script src="js/holder.js"></script>
</script>
</body>
</html>