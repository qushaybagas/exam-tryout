
<!-- Modal Soal Form -->
<div id="ModalSoal" >
  <div class="modal-header">
    <h3>Tambah Soal</h3>
  </div>
  <div class="modal-body">
    <form>
        <table>
          <tr>
            <td class="span6">
              <textarea class="span5" rows="3" placeholder="Soal"></textarea>
            </td>
            <td class="span4">
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input type="file" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span5" rows="1" placeholder="Pilihan Jawaban"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new">Select <i class="iconic-image"></i></span>
                <span class="fileupload-exists">Change <i class="iconic-image"></i></span><input type="file" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="iconic-trash"></i></a>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span5" rows="1" placeholder="Pilihan Jawaban"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new">Select <i class="iconic-image"></i></span>
                <span class="fileupload-exists">Change <i class="iconic-image"></i></span><input type="file" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="iconic-trash"></i></a>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span5" rows="1" placeholder="Pilihan Jawaban"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new">Select <i class="iconic-image"></i></span>
                <span class="fileupload-exists">Change <i class="iconic-image"></i></span><input type="file" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="iconic-trash"></i></a>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span5" rows="1" placeholder="Pilihan Jawaban"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new">Select <i class="iconic-image"></i></span>
                <span class="fileupload-exists">Change <i class="iconic-image"></i></span><input type="file" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="iconic-trash"></i></a>
              </div>
            </td>
          </tr>
        </table>
        
        
     
      
    </form>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary">Save</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>