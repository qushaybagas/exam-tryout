<?php
 session_start();
 if (!isset($_SESSION['user'])){
    header("Location:index.php");
  }
  require("conn.php");
  $mapel="MTK";
  $kode_soal=1;
  if (isset($_GET['kode'])&&isset($_GET['mapel'])){
    $kode_soal=$_GET['kode'];
    $mapel=$_GET['mapel'];
  }
  if (isset($_GET['tips_del'])){
    $tips_del=$_GET['tips_del'];
    $sql_tips_delete="DELETE FROM t_tips WHERE id_tips='".$tips_del."'";
    mysql_query($sql_tips_delete);
    header( 'Location: tips.php') ;
  }

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Admin Tryout</title>
    <meta name="description" content="">
    <meta name="author" content="">

    
    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/bootstrap-fileupload.min.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="css/prettify.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="#">Admin Tryout</a>
      <div class="nav-collapse">
        <ul class="nav">
          <li class="dropdown"  class="active">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mata Pelajaran <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="mat.php">Matematika</a></li>
              <li><a href="ipa.php">IPA</a></li>
              <li><a href="bin.php">Bahasa Indonesia</a></li>
              <li><a href="big.php">Bahasa Inggris</a></li>
            </ul>
          </li>  
          <li><a href="materi.php">Materi</a></li> 
          <li><a href="tips.php">Tips</a></li>     
          <li><a href="user.php">Pengguna</a></li> 
        </ul>
      </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
  </div><!-- /.navbar-inner -->
</div><!-- /.navbar -->
</div>

<div class="container">

<!-- Modal Soal Form -->
<?php

  if(isset($_GET['id_tips'])){
    $sql_tips_edit="SELECT * FROM t_tips WHERE id_tips='$id_tips'";
    $hasil_tips_edit=mysql_query($sql_tips_edit);
    $data_tips_edit=mysql_fetch_array($hasil_tips_edit);
  }

?>

  <div class="modal-header">
    <h3>Tambah tips</h3>
  </div>

  <form action="tips.php" method="post">
  <div class="modal-body">
        <table>
          <tr>
            <td class="span10">
              <textarea class="span9" rows="1" placeholder="Judul tips" name="judul_tips"></textarea>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span9" rows="5" placeholder="Isi tips" name="isi_tips"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new"><i class="icon-folder-open"></i></span><span class="fileupload-exists"><i class="icon-folder-close"></i></span><input type="file" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i></a>
              </div>
            </td>
          </tr>
        </table>
   
  </div>
  <div class="modal-footer">
    <button type="submit" name="input_tips_aksi" value="save" class="btn btn-primary">Save</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
   </form>

<!-- Modal Soal Form -->
<div class="modal hide" id="ModalTipsEdit" aria-hidden="true">
  <div class="modal-header">
    <h3>Edit tips</h3>
  </div>

  <form action="tips.php" method="post">
  <div class="modal-body">
        <table>
          <tr>
            <td >
              <input type="hidden" name="id_tips" value=<?php echo $data_tips_edit['id_tips'];  ?>>
              <textarea class="span5" rows="1" name="judul_tips"><?php echo $data_tips_edit['bab']; ?>
              </textarea>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span5" rows="5" name="isi_tips"><?php echo $data_tips_edit['tips']; ?>
              </textarea>
            </td>
          </tr>
        </table>
   
  </div>
  <div class="modal-footer">
    <button type="submit" name="input_tips_aksi" value="edit" class="btn btn-primary">Edit</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
   </form>
</div>

<script type="text/javascript" src="js/widgets.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-fileupload.min.js"></script>
<script src="js/application.js"></script>
<script src="js/holder.js"></script>
</script>
</body>
</html>