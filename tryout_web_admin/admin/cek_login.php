<?php
    require_once 'include/DB_Functions.php';
    $db = new DB_Functions();
    if (isset($_POST['user']) && isset($_POST['password'])) {
        // Request type is check Login
        $user = $_POST['user'];
        $password = $_POST['password'];

        // check for user
        $user = $db->getUserByEmailAndPassword($user, $password);
        if ($user != false) {
        	header("Location:admin.php");
        } else {
        	header("Location:index.php");
        }
    } else {
        header("Location:index.php");
    }
?>
