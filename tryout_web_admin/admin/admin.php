<?php
  require("conn.php");
  $sql="SELECT * FROM t_soal WHERE id_mapel='MTK'";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Admin Tryout</title>
    <meta name="description" content="">
    <meta name="author" content="">

    
    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="css/prettify.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="#">Admin Tryout</a>
      <div class="nav-collapse">
        <ul class="nav">
          <li class="active"><a href="#">Matematika</a></li>
          <li><a href="#">IPA</a></li>
          <li><a href="#">Bahasa Indonesia</a></li>
          <li><a href="#">Bahasa Inggris</a></li>
        </ul>
      </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
  </div><!-- /.navbar-inner -->
</div><!-- /.navbar -->
</div>

<div class="pagination pagination-centered pagination-large">
  <ul>
    <li class="disabled"><a href="#">«</a></li>
    <li class="active"><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li class="disabled"><a href="#">»</a></li>
  </ul>
</div>


<div class="container">
    <!-- nomer soal -->
    <div class="span1 bs-docs-sidebar">
      <ul class="nav nav-list bs-docs-sidenav">
        <?php
          $jml_soal=mysql_num_rows(mysql_query($sql));
          for ($i=1;$i<=$jml_soal;$i++){
           echo "<li><a href='#".$i."'>".$i."</a></li>";
          }
        ?>
      </ul>
    </div>

    <div class="span9">

    <script>
      $("#blob").popover({
      placement : 'bottom', //placement of the popover. also can use top, bottom, left or right
      title : 'sss', //this is the top title bar of the popover. add some basic css
      html: 'true', //needed to show html of course
      content : 's' //this is the content of the html box. add the image here or anything you want really.

      });
    </script>
         
         <?php

            $hasil=mysql_query($sql);
            $no=1;
            while($data=mysql_fetch_array($hasil)){
              echo "<section id='".$no."'>";

              $sql_pilihan="SELECT * FROM t_pilihan WHERE id_soal='".$data['id_soal']."'";
              $hasil_pilihan=mysql_query($sql_pilihan);
              $hasil_pilihan_cekbenar=mysql_query($sql_pilihan);
              echo "<div class='bs-docs-example'>";
              echo "<div class='label-nomer-soal'>No. ".$no."</div>";
              echo "<div class='menu-soal'><a href='#ModalSolusi".$no."' data-toggle='modal' class='btn btn-mini btn-info'>Solusi</a> 
              <a href='#' rel='popdel'><i class='icon-edit'></i></a>  
              <a href='#' rel='popdel'><i class='icon-trash'></i></a></div>";

              if ($data['soal']!=null){
                echo $data['soal'];
              }else{
                echo "<img src='client/soal/".$data['gambar_soal'].".png' class='img-rounded'>";
           
              }
              
              echo "</div>";
              echo "<pre class='prettyprint'>";
              echo "<div class='tabbable tabs-left'>";
              $abcd=array('A','B','C','D');
              $iabcd=0;
              echo "<ul class='nav nav-tabs'>";
              while($data_pilihan_cekbenar=mysql_fetch_array($hasil_pilihan_cekbenar)){
                if ($data_pilihan_cekbenar['stat']=='b'){
                  echo "<li><a href='#".$no."".$abcd[$iabcd]."' data-toggle='tab'><i class='icon-thumbs-up'></i></a></li>";
                }else{
                  echo "<li><a href='#".$no."".$abcd[$iabcd]."' data-toggle='tab'>".$abcd[$iabcd]."</a></li>";
                }
                $iabcd++;
              }
              echo "</ul>";
              echo "<div class='tab-content'>";
              $iabcd=0;
              while($data_pilihan=mysql_fetch_array($hasil_pilihan)){
                if ($data_pilihan['pilihan']!=null){
                  $isi_pilihan=$data_pilihan['pilihan'];
                }else{
                  $isi_pilihan="<img src='client/jawaban/".$data_pilihan['gambar_pilihan'].".png'>";
                }
                echo "<div class='tab-pane' id='".$no."".$abcd[$iabcd]."'><p>".$isi_pilihan."</p></div>";
                $iabcd++;
              }
              echo "</div>";
              echo "</div>";
              echo "</pre>";
              echo "</section>";
              echo "<div class='modal hide' id='ModalSolusi".$no."' aria-hidden='true'>";
              echo "<div class='modal-header'> <h5>Solusi</h5></div><div class='modal-body'>";
              $str_solusi=str_replace("#", "<br>", $data['solusi']);
              echo $str_solusi;
              echo "</div></div>";
              $no++;
            }
          ?>  

      <a href="#ModalSoal" data-toggle="modal" class="btn btn-primary"><i class="icon-plus icon-white"></i> Soal</a>
    </div>
</div>

<!-- Modal Soal Form -->
<div class="modal hide" id="ModalSoal" aria-hidden="true">
  <div class="modal-header">
    <h3>Tambah Soal</h3>
  </div>
  <div class="modal-body">
    <form>
        <table>
          <tr>
            <td class="span6">
              <textarea class="span5" rows="3" placeholder="Soal"></textarea>
            </td>
            <td class="span4">
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new">Select <i class="iconic-image"></i></span>
                <span class="fileupload-exists">Change <i class="iconic-image"></i></span><input type="file" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="iconic-trash"></i></a>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span5" rows="1" placeholder="Pilihan Jawaban"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new">Select <i class="iconic-image"></i></span>
                <span class="fileupload-exists">Change <i class="iconic-image"></i></span><input type="file" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="iconic-trash"></i></a>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span5" rows="1" placeholder="Pilihan Jawaban"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new">Select <i class="iconic-image"></i></span>
                <span class="fileupload-exists">Change <i class="iconic-image"></i></span><input type="file" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="iconic-trash"></i></a>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span5" rows="1" placeholder="Pilihan Jawaban"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new">Select <i class="iconic-image"></i></span>
                <span class="fileupload-exists">Change <i class="iconic-image"></i></span><input type="file" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="iconic-trash"></i></a>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span5" rows="1" placeholder="Pilihan Jawaban"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file"><span class="fileupload-new">Select <i class="iconic-image"></i></span>
                <span class="fileupload-exists">Change <i class="iconic-image"></i></span><input type="file" /></span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="iconic-trash"></i></a>
              </div>
            </td>
          </tr>
        </table>
        
        
     
      
    </form>
  </div>
  <div class="modal-footer">
    <button class="btn btn-primary">Save</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>

<script type="text/javascript" src="js/widgets.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/prettify.js"></script>
<script src="js/application.js"></script>
<script src="js/holder/holder.js"></script>
<script>
      // Activate Google Prettify in this page
      addEventListener('load', prettyPrint, false);
    
      $(document).ready(function(){

        // Add prettyprint class to pre elements
          $('pre').addClass('prettyprint');

        // Initialize tabs and pills
          $('.note-tabs').tab();
                
      }); // end document.ready
</script>
<script type="text/javascript">
 $(document).ready(function() {
  $("[rel=popdel]").popover({
      placement : 'bottom', //placement of the popover. also can use top, bottom, left or right
      title : '<div style="text-align:center; color:red; font-size:14px;"> Delete?</div>', //this is the top title bar of the popover. add some basic css
      html: 'true', //needed to show html of course
      content : '<a type="button" class="btn btn-mini btn-danger">Delete</a>' //this is the content of the html box. add the image here or anything you want really.
});
});
</script>

</body>
</html>