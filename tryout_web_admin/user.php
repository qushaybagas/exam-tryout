<?php
session_start();
 if (!isset($_SESSION['user'])){
    header("Location:index.php");
  }
  require("conn.php");

  $dateNow=date('Y-m-d H:i:s');

  if (isset($_POST['nama_user'])&&isset($_POST['email_user'])&&isset($_POST['pass_user'])){
    $nama_user=$_POST['nama_user'];
    $email_user=$_POST['email_user'];
    $pass_user=$_POST['pass_user'];
    if ($_POST['save']){
      $sql_user_input="INSERT INTO t_user (name,email,encrypted_password,created_at)
      VALUES
      ('$nama_user','$email_user','$pass_user','$dateNow')";
      mysql_query($sql_user_input);
    }elseif ($_POST['edit']){
      $id_user=$_POST['id_user'];
      $sql_user_edit="UPDATE t_user SET bab='$judul_user', user='$isi_user' WHERE id_user='$id_user'";
      mysql_query($sql_user_edit);
    }elseif ($_POST['delete']){
      $sql_user_delete="DELETE FROM t_user WHERE id_user='4'";
      mysql_query($sql_user_delete);
    }
  }


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Admin Tryout</title>
    <meta name="description" content="">
    <meta name="author" content="">

    
    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="css/prettify.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="#">Admin Tryout</a>
      <div class="nav-collapse">
        <ul class="nav">
          <li class="dropdown"  class="active">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mata Pelajaran <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="mat.php">Matematika</a></li>
              <li><a href="ipa.php">IPA</a></li>
              <li><a href="bin.php">Bahasa Indonesia</a></li>
              <li><a href="big.php">Bahasa Inggris</a></li>
            </ul>
          </li>  
          <li><a href="materi.php">Materi</a></li> 
          <li><a href="tips.php">Tips</a></li>     
          <li class="active"><a href="user.php">Pengguna</a></li> 
        </ul>
        
        <ul class="pull-right nav">
          <li><a href="logout.php"><i class="icon-off"></i> Logout</a></li> 
        </ul>
      </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
  </div><!-- /.navbar-inner -->
</div><!-- /.navbar -->
</div>

<div class="container">

  <p style="margin-top:10px;">
    <a href="input_user.php" class="btn btn-primary"><i class="icon-plus icon-white"></i> user</a>
  </p>
  <div class="accordion" id="accordion2">
    <?php
    $sql_user="SELECT * FROM t_user";
    $hasil_user=mysql_query($sql_user);
    $iuser=1;
    echo "<table class='table table-bordered'>";
    echo "<thead>";
      echo "  <tr>";
      echo "    <th>Nama</th>";
      echo "    <th>Email</th>";
      echo "    <th>Password</th>";
      echo "    <th>Tanggal dibuat</th>";
      echo "  </tr>";
      echo "</thead>";
      echo "<tbody>";
    while($data_user=mysql_fetch_array($hasil_user)){
      $id_user=$data_user['uid'];
      
      echo "  <tr>";
      echo "    <td>".$data_user['name']."</td>";
      echo "    <td>".$data_user['email']."</td>";
      echo "    <td>".$data_user['encrypted_password']."</td>";
      echo "    <td>".$data_user['created_at']."</td> ";
      echo "    <td style='background-color:#FEF; width:20px;'>
                <center>
                <a href='input_user.php?user_del=".$id_user."'><i class='icon-trash'></i></a>
                </center>
                </td>";
      echo "  </tr>";
    }
    echo "</tbody>";
    echo "</table>";
    ?>
  </div>
</div>

<script type="text/javascript" src="js/widgets.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/application.js"></script>
<script src="js/holder.js"></script>
</body>
</html>