<?php
$response = array();
if (isset($_GET['mapel'])) {
    $mapel = $_GET['mapel'];
    // include db connect class
    require_once 'include/DB_Connect.php';

    // connecting to db
    $db = new DB_Connect();

    // get all products from products table
    $result = mysql_query("SELECT DISTINCT(set_soal) AS kode FROM t_soal WHERE id_mapel='$mapel' ORDER BY kode ASC") or die(mysql_error());

    // check for empty result
    if (mysql_num_rows($result) > 0) {
        $response["kode"]=rand(1,mysql_num_rows($result));
        $response["success"] = 1;
        echo json_encode($response);       
    } else {
        // no products found
        $response["success"] = 0;
        $response["message"] = "Belum ada tips!";

        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>
