<?php

/*
 * Following code will list all the products
 */

// array for JSON response
$response = array();
if (isset($_POST['mapel'])) {
    $mapel = $_POST['mapel'];
    // include db connect class
    require_once 'include/DB_Connect.php';

    // connecting to db
    $db = new DB_Connect();

    // get all products from products table
    $result = mysql_query("SELECT * FROM t_materi WHERE id_mapel='$mapel'") or die(mysql_error());

    // check for empty result
    if (mysql_num_rows($result) > 0) {
        // looping through all results
        // products node
        $response["user"] = array();
        
        while ($row = mysql_fetch_array($result)) {
            // temp user array
            $product = array();
            $product["pid"] = $row["id_materi"];
            $product["bab"] = $row["bab"];

            // push single product into final response array
            array_push($response["user"], $product);
        }
        // success
        $response["success"] = 1;

        // echoing JSON response
        echo json_encode($response);
    } else {
        // no products found
        $response["success"] = 0;
        $response["message"] = "No products found";

        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>
