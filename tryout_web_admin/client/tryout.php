<?php

/*
 * Following code will list all the products
 */

// array for JSON response
$response = array();
if (isset($_POST['mapel'])) {
    $mapel = $_POST['mapel'];
    // include db connect class
    require_once 'include/DB_Connect.php';

    // connecting to db
    $db = new DB_Connect();

    // get all products from products table 
    $result = mysql_query("SELECT * FROM t_soal WHERE id_mapel='$mapel' ORDER BY RAND() LIMIT 25") or die(mysql_error());

    // check for empty result
    if (mysql_num_rows($result) > 0) {
        // looping through all results
        // products node
        $response["user"] = array();
        
        while ($row = mysql_fetch_array($result)) {
            // temp user array
            $product = array();
            $product["pid"] = $row["id_soal"];
            $product["soal"] = $row["soal"];
            $product["gbr_soal"] = $row["gambar_soal"];
            $idsoal=$product['pid'];
            $result_pil = mysql_query("SELECT * FROM t_pilihan WHERE id_soal='$idsoal'") or die(mysql_error());
            $ipil=1;
            while ($row2 = mysql_fetch_array($result_pil)) {
               // $product["pidpil$ipil"]=$row2["id_pilihan"];
                $product["stat$ipil"]=$row2["stat"];
                $product["pil$ipil"]=$row2["pilihan"];
                $product["gbr_pil$ipil"]=$row2["gambar_pilihan"];
                $ipil=$ipil+1;
            }
            // push single product into final response array
            array_push($response["user"], $product);
        }
        // success
        $response["success"] = 1;

        // echoing JSON response
        echo json_encode($response);
    } else {
        // no products found
        $response["success"] = 0;
        $response["message"] = "No products found";

        // echo no users JSON
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";

    // echoing JSON response
    echo json_encode($response);
}
?>
