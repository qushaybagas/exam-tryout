<?php
 session_start();
 if (!isset($_SESSION['user'])){
    header("Location:index.php");
  }
  require("conn.php");
  if (isset($_GET['mapel'])){
    $materi_mapel=$_GET['mapel'];
  }else{
    $materi_mapel="mtk";
  }

  if (isset($_GET['materi_del'])){
    $materi_del=$_GET['materi_del'];
    $materi_mapel=$_GET['mapel'];
    
    $sql_materi_lihat_gambar="SELECT gbr_materi FROM t_materi WHERE id_materi='$materi_del'";
    $query_materi_lihat_gambar=mysql_query($sql_materi_lihat_gambar);
    $gambar_materi=mysql_fetch_array($query_materi_lihat_gambar);
    if (file_exists("client/materi/".$gambar_materi['gbr_materi'].".png")){
    	unlink("client/materi/".$gambar_materi['gbr_materi'].".png");
    }
    
    $sql_materi_delete="DELETE FROM t_materi WHERE id_materi='".$materi_del."'";
    mysql_query($sql_materi_delete);
    
    header( 'Location: materi.php?mapel='.$materi_mapel ) ;
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Admin Tryout</title>
    <meta name="description" content="">
    <meta name="author" content="">

    
    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="css/prettify.css" rel="stylesheet">
    <link href="css/bootstrap-fileupload.min.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="#">Admin Tryout</a>
      <div class="nav-collapse">
        <ul class="nav">
          <li class="dropdown"  class="active">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mata Pelajaran <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="mat.php">Matematika</a></li>
              <li><a href="ipa.php">IPA</a></li>
              <li><a href="bin.php">Bahasa Indonesia</a></li>
              <li><a href="big.php">Bahasa Inggris</a></li>
            </ul>
          </li>  
          <li class="active"><a href="materi.php">Materi</a></li> 
          <li><a href="tips.php">Tips</a></li>     
          <li><a href="user.php">Pengguna</a></li> 
        </ul>
      </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
  </div><!-- /.navbar-inner -->
</div><!-- /.navbar -->
</div>


<div class="container">

  <?php

  if(isset($_GET['mapel'])){
    $input_materi_mapel=$_GET['mapel'];

  }
  if(isset($_GET['id_materi'])){
    $sql_materi_edit="SELECT * FROM t_materi WHERE id_materi='$id_materi'";
    $hasil_materi_edit=mysql_query($sql_materi_edit);
    $data_materi_edit=mysql_fetch_array($hasil_materi_edit);
  }

?>
  <div class="modal-header">
    <h3>Tambah Materi</h3>
  </div>

  <form action="materi.php" method="post" enctype="multipart/form-data">
  <div class="modal-body">
        <table>
          <tr>
            <td class="span10">
              <textarea class="span9" rows="1" placeholder="Judul Materi" name="judul_materi"></textarea>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span9" rows="5" placeholder="Isi Materi" name="isi_materi"></textarea>
            </td>
            <td>
              <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-new thumbnail" style="width: 50px; height: 50px;"><img src="http://www.placehold.it/50x50/EFEFEF/AAAAAA" /></div>
                <div class="fileupload-preview fileupload-exists thumbnail" style="width: 50px; height: 50px;"></div>
                <span class="btn btn-file">
                	<span class="fileupload-new"><i class="icon-folder-open"></i></span>
                	<span class="fileupload-exists"><i class="icon-folder-close"></i></span>
                	<input type="file"  name="input_materi_gbr" />
                </span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i></a>
              </div>
            </td>
          </tr>
        </table>
   
  </div>
  <div class="modal-footer">
    <input type="hidden" name="input_materi_mapel" value="<?php echo $input_materi_mapel ?>"/>
    <input type="hidden" name="input_materi_aksi" value="save"/>
    <button type="submit" name="save" value="save" class="btn btn-primary">Save</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
   </form>
<!-- Modal Soal Form -->
<div class="modal hide" id="ModalMateriEdit" aria-hidden="true">
  <div class="modal-header">
    <h3>Edit Materi</h3>
  </div>

  <form action="materi.php" method="post">
  <div class="modal-body">
        <table>
          <tr>
            <td >
              <input type="hidden" name="id_materi" value=<?php echo $data_materi_edit['id_materi'];  ?>>
              <textarea class="span5" rows="1" name="judul_materi"><?php echo $data_materi_edit['bab']; ?>
              </textarea>
            </td>
          </tr>
          <tr>
            <td>
              <textarea class="span5" rows="5" name="isi_materi"><?php echo $data_materi_edit['materi']; ?>
              </textarea>
            </td>
          </tr>
        </table>
   
  </div>
  <div class="modal-footer">
    <button type="submit" name="edit" value="edit" class="btn btn-primary">Edit</button>
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
   </form>
</div>
</div>

<script type="text/javascript" src="js/widgets.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-fileupload.min.js"></script>
<script src="js/application.js"></script>
<script src="js/holder.js"></script>
</body>
</html>