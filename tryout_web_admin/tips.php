<?php
session_start();
 if (!isset($_SESSION['user'])){
    header("Location:index.php");
  }
  require("conn.php");

  if (isset($_POST['judul_tips'])&&isset($_POST['isi_tips'])){
    $judul_tips=$_POST['judul_tips'];
    $isi_tips=$_POST['isi_tips'];
    if ($_POST['input_tips_aksi']=="save"){
      $sql_tips_input="INSERT INTO t_tips (title,isi)
      VALUES
      ('$judul_tips','$isi_tips')";
      mysql_query($sql_tips_input);
    }elseif ($_POST['input_tips_aksi']=="edit"){
      $id_tips=$_POST['id_tips'];
      $sql_tips_edit="UPDATE t_tips SET bab='$judul_tips', tips='$isi_tips' WHERE id_tips='$id_tips'";
      mysql_query($sql_tips_edit);
    }
  }


?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Admin Tryout</title>
    <meta name="description" content="">
    <meta name="author" content="">

    
    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-responsive.css" rel="stylesheet">
    <link href="css/docs.css" rel="stylesheet">
    <link href="css/prettify.css" rel="stylesheet">
</head>
<body data-spy="scroll" data-target=".bs-docs-sidebar">
<div>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="#">Admin Tryout</a>
      <div class="nav-collapse">
        <ul class="nav">
          <li class="dropdown"  class="active">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mata Pelajaran <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="mat.php">Matematika</a></li>
              <li><a href="ipa.php">IPA</a></li>
              <li><a href="bin.php">Bahasa Indonesia</a></li>
              <li><a href="big.php">Bahasa Inggris</a></li>
            </ul>
          </li>  
          <li><a href="materi.php">Materi</a></li> 
          <li class="active"><a href="tips.php">Tips</a></li>     
          <li><a href="user.php">Pengguna</a></li> 
        </ul>
        
        <ul class="pull-right nav">
          <li><a href="logout.php"><i class="icon-off"></i> Logout</a></li> 
        </ul>
      </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
  </div><!-- /.navbar-inner -->
</div><!-- /.navbar -->
</div>

<div class="container">

  <p style="margin-top:10px;">
    <a href="input_tips.php" class="btn btn-primary"><i class="icon-plus icon-white"></i> Tips</a>
  </p>
  <div class="accordion" id="accordion2">
    <?php
    $sql_tips="SELECT * FROM t_tips";
    $hasil_tips=mysql_query($sql_tips);
    $iTips=1;
    while($data_tips=mysql_fetch_array($hasil_tips)){
    $id_tips=$data_tips['id_tips'];
    echo "<div class='accordion-group' id_tips=".$id_tips.">";
    echo "  <div class='accordion-heading'>";
    echo "    <a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion2' href='#collapse".$iTips."'>";
    echo          $data_tips['title'];
    echo "    </a>";
    echo "    <div class='accordion-heading-menu'>
                <a href='input_tips.php?tips_del=".$id_tips."'><i class='icon-trash'></i></a>
              </div>";
    echo "  </div>";
    echo "  <div id='collapse".$iTips."' class='accordion-body collapse'>
              <div class='accordion-inner'>";
    echo          $data_tips['isi'];
    echo "    </div>
            </div>
          </div>";
    $iTips++;
    }
    ?>
  </div>
</div>

<script type="text/javascript" src="js/widgets.js"></script>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/application.js"></script>
<script src="js/holder.js"></script>
</body>
</html>